// (c) 2016 - 2019: Marcel Admiraal

#include "imudatacollector.h"

#include <wx/utils.h>

#include "ximuapi.h"
#include "nuapi.h"
#include "nufiledataapi.h"
#include "nufilerawapi.h"

#include <fstream>

#define AXES 3
#define CHANNELS 8

namespace ma
{
    IMUDataCollector::IMUDataCollector() :
        imus(0), imuTypeIndex(0), imuapi(0), listener(0),
        fileListener(0), logger(0), waitingForRegisterData(false),
        usingQuaternions(true), usingAccelerometers(false),
        usingGyroscopes(false), usingMagnetometers(false), usingAnalogue(false),
        historyLength(new unsigned int[5]()), sampleFrequency(1000),
        quaternionHistory (0), accelerometerHistory(0), gyroscopeHistory(0),
        magnetometerHistory(0), analogueHistory(0), signal(0),
        accelerometerFilter(0), gyroscopeFilter(0), magnetometerFilter(0),
        analogueFilter(0),
        masterAccelerometerFilter(AXES), masterGyroscopeFilter(AXES),
        masterMagnetometerFilter(AXES), masterAnalogueFilter(CHANNELS)
    {
        if (!loadIMUSettings(wxT("imusettings.dat")))
        {
            for (unsigned int i = 0; i < 5; ++i)
                historyLength[i] = 1;
            setIMUs(1);
        }
    }

    IMUDataCollector::~IMUDataCollector()
    {
        setIMUs(0);
    }

    Vector IMUDataCollector::getAllValues()
    {
        if (imus == 0) return Vector();
        Vector currentValue(getAllValuesSize());

        wxMutexLocker valueLock(valueAccess);
        unsigned int offset = 0;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            for (unsigned int value = 0; value < 4; ++value)
            {
                 currentValue.setSubVector(offset,
                        quaternionHistory[imu].getData(value));
                offset += quaternionHistory[imu].getDataLength();
            }
            for (unsigned int value = 0; value < AXES; ++value)
            {
                 currentValue.setSubVector(offset,
                        accelerometerHistory[imu].getData(value));
                offset += accelerometerHistory[imu].getDataLength();
            }
            for (unsigned int value = 0; value < AXES; ++value)
            {
                 currentValue.setSubVector(offset,
                        gyroscopeHistory[imu].getData(value));
                offset += gyroscopeHistory[imu].getDataLength();
            }
            for (unsigned int value = 0; value < AXES; ++value)
            {
                 currentValue.setSubVector(offset,
                        magnetometerHistory[imu].getData(value));
                offset += magnetometerHistory[imu].getDataLength();
            }
            for (unsigned int value = 0; value < CHANNELS; ++value)
            {
                 currentValue.setSubVector(offset,
                        analogueHistory[imu].getData(value));
                offset += analogueHistory[imu].getDataLength();
            }
        }
        return currentValue;
    }

    unsigned int IMUDataCollector::getAllValuesSize()
    {
        wxMutexLocker valueLock(valueAccess);
        if (imus == 0) return 0;
        unsigned int imuParameters =
            quaternionHistory[0].getDataLength() *
                quaternionHistory[0].getChannels() +
            accelerometerHistory[0].getDataLength() *
                accelerometerHistory[0].getChannels() +
            gyroscopeHistory[0].getDataLength() *
                gyroscopeHistory[0].getChannels() +
            magnetometerHistory[0].getDataLength() *
                magnetometerHistory[0].getChannels() +
            analogueHistory[0].getDataLength() *
                analogueHistory[0].getChannels();
        return imuParameters * imus;
    }

    Quaternion IMUDataCollector::getCurrentQuaternion(const unsigned int imu)
    {
        wxMutexLocker valueLock(valueAccess);
        if (imu < imus)
        {
            Vector quaternionVector = getCurrentQuaternionVector(imu);
            Quaternion quaternion(
                quaternionVector[0],
                quaternionVector[1],
                quaternionVector[2],
                quaternionVector[3]);
            if (quaternion == Quaternion()) quaternion.setW(1);
            return quaternion;
        }
        else
            return Quaternion();
    }

    Vector IMUDataCollector::getCurrentQuaternionVector(const unsigned int imu)
    {
        wxMutexLocker valueLock(valueAccess);
        if (imu < imus)
            return quaternionHistory[imu].getLastData();
        else
            return Vector(4);
    }

    Vector IMUDataCollector::getCurrentAccelerometer(const unsigned int imu)
    {
        wxMutexLocker valueLock(valueAccess);
        if (imu < imus)
            return accelerometerHistory[imu].getLastData();
        else
            return Vector(AXES);
    }

    Vector IMUDataCollector::getCurrentGyroscope(const unsigned int imu)
    {
        wxMutexLocker valueLock(valueAccess);
        if (imu < imus)
            return gyroscopeHistory[imu].getLastData();
        else
            return Vector(AXES);
    }

    Vector IMUDataCollector::getCurrentMagnetometer(const unsigned int imu)
    {
        wxMutexLocker valueLock(valueAccess);
        if (imu < imus)
            return magnetometerHistory[imu].getLastData();
        else
            return Vector(AXES);
    }

    Vector IMUDataCollector::getCurrentAnalogueData(const unsigned int imu)
    {
        wxMutexLocker valueLock(valueAccess);
        if (imu < imus)
            return analogueHistory[imu].getLastData();
        else
            return Vector(CHANNELS);
    }

    unsigned int IMUDataCollector::getCurrentSignal(const unsigned int imu)
    {
        wxMutexLocker valueLock(valueAccess);
        if (imu < imus)
            return signal[imu];
        else
            return 0;
    }

    Data* IMUDataCollector::getRegisterData(const unsigned int imu)
    {
        if (imu >= imus) return 0;

        waitingForRegisterData = true;
        imuapi[imu]->getRegisters();
        double start = std::clock();
        while (waitingForRegisterData
                && (std::clock() - start)/CLOCKS_PER_SEC < 1);
        if (!waitingForRegisterData)
            return new Data(registerData);
        else
            return 0;
    }

    void IMUDataCollector::setRegisterData(const unsigned int imu,
            Data* registerData)
    {
        imuapi[imu]->setRegisters(registerData);
    }

    bool IMUDataCollector::saveRegistersToFlash(const unsigned int imu)
    {
        if (imu >= imus) return false;
        nu::API* nuapi = dynamic_cast<nu::API*>(imuapi[imu]);
        if (!nuapi) return false;
        return nuapi->saveRegistersToFlash();
    }

    unsigned int IMUDataCollector::getIMUs() const
    {
        return imus;
    }

    void IMUDataCollector::setIMUs(const unsigned int newImus)
    {
        unsigned int* newIMUTypeIndex;
        IMUAPI** newIMUAPI;
        IMUCollectorListener** newListener;
        IMUCollectorFileListener** newFileListener;
        if (newImus > 0)
        {
            newIMUTypeIndex = new unsigned int[newImus];
            newIMUAPI = new IMUAPI*[newImus];
            newListener = new IMUCollectorListener*[newImus];
            newFileListener = new IMUCollectorFileListener*[newImus]();
        }
        // Copy existing objects.
        for (unsigned int imu = 0; imu < imus && imu < newImus; ++imu)
        {
            newIMUTypeIndex[imu] = imuTypeIndex[imu];
            newIMUAPI[imu] = imuapi[imu];
            newListener[imu] = listener[imu];
            newFileListener[imu] = fileListener[imu];
        }
        // Create new objects.
        for (unsigned int imu = imus; imu < newImus; ++imu)
        {
            newIMUTypeIndex[imu] = 0;
            newIMUAPI[imu] = new xIMU::API();
            newListener[imu] = new CollectorxIMUListener(this, imu);
            newIMUAPI[imu]->addListener(newListener[imu]);
        }
        // Delete old objects.
        for (unsigned int imu = newImus; imu < imus; ++imu)
        {
            imuapi[imu]->removeListener(listener[imu]);
            if (fileListener[imu] != 0)
                imuapi[imu]->removeListener(fileListener[imu]);
            delete imuapi[imu];
            delete listener[imu];
            if (fileListener[imu] != 0)
            {
                delete fileListener[imu];
                fileListener[imu] = 0;
            }
        }
        // Swap arrays.
        std::swap(imuTypeIndex, newIMUTypeIndex);
        std::swap(imuapi, newIMUAPI);
        std::swap(listener, newListener);
        std::swap(fileListener, newFileListener);
        // Clean up.
        if (imus > 0)
        {
            delete[] newIMUTypeIndex;
            delete[] newIMUAPI;
            delete[] newListener;
            delete[] newFileListener;
        }
        // Update storage vectors and filters.
        wxMutexLocker valueLock(valueAccess);
        if (imus > 0)
        {
            delete[] quaternionHistory;
            delete[] accelerometerHistory;
            delete[] gyroscopeHistory;
            delete[] magnetometerHistory;
            delete[] analogueHistory;
            delete[] signal;
            delete[] accelerometerFilter;
            delete[] gyroscopeFilter;
            delete[] magnetometerFilter;
            delete[] analogueFilter;
        }
        if (newImus > 0)
        {
            quaternionHistory = new TemporalData[newImus];
            accelerometerHistory = new TemporalData[newImus];
            gyroscopeHistory = new TemporalData[newImus];
            magnetometerHistory = new TemporalData[newImus];
            analogueHistory = new TemporalData[newImus];
            signal = new unsigned int[newImus]();
            for (unsigned int imu = 0; imu < newImus; ++imu)
            {
                quaternionHistory[imu] = TemporalData(4, sampleFrequency,
                    historyLength[0]);
                accelerometerHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength[1]);
                gyroscopeHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength[2]);
                magnetometerHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength[3]);
                analogueHistory[imu] = TemporalData(CHANNELS, sampleFrequency,
                    historyLength[4]);
            }
            accelerometerFilter = new MultiFilter[newImus];
            gyroscopeFilter = new MultiFilter[newImus];
            magnetometerFilter = new MultiFilter[newImus];
            analogueFilter = new MultiFilter[newImus];
        }
        imus = newImus;
        replaceFilters();
    }

    const unsigned int IMUDataCollector::getIMUTypes() const
    {
        return imuTypes;
    }

    const wxString* IMUDataCollector::getIMUTypeNames() const
    {
        return imuTypeNames;
    }

    wxString IMUDataCollector::getIMUTypeName(const unsigned int imu) const
    {
        return imuTypeNames[imuTypeIndex[imu]];
    }

    void IMUDataCollector::setIMUType(const unsigned int imuType,
            const unsigned int imu)
    {
        imuTypeIndex[imu] = imuType;
        imuapi[imu]->removeListener(listener[imu]);
        if (fileListener[imu] != 0)
            imuapi[imu]->removeListener(fileListener[imu]);
        Str portName = imuapi[imu]->getPort();
        delete imuapi[imu];
        delete listener[imu];
        if (fileListener[imu] != 0)
        {
            delete fileListener[imu];
            fileListener[imu] = 0;
        }
        switch (imuType)
        {
        case 0:
            imuapi[imu] = new xIMU::API();
            listener[imu] = new CollectorxIMUListener(this, imu);
            break;
        case 1:
            imuapi[imu] = new nu::API();
            listener[imu] = new CollectornuListener(this, imu);
            break;
        case 2:
            imuapi[imu] = new nu::FileDataAPI(imu);
            listener[imu] = new CollectornuListener(this, imu);
            break;
        case 3:
            imuapi[imu] = new nu::FileRawAPI(imu);
            listener[imu] = new CollectornuListener(this, imu);
            fileListener[imu] = new IMUCollectorFileListener(this, imu);
            imuapi[imu]->addListener(fileListener[imu]);
            break;
        }
        imuapi[imu]->addListener(listener[imu]);
        imuapi[imu]->setPort(portName);
    }

    bool IMUDataCollector::saveIMUsettings(const wxString& filename)
    {
        std::ofstream fileStream(filename.c_str(), std::ofstream::out);
        if (fileStream.is_open())
        {
            fileStream << "ISv4" << std::endl;
            fileStream << imus << std::endl;
            for (unsigned imu = 0; imu < imus; ++imu)
            {
                fileStream << imuTypeIndex[imu];
                fileStream << imuapi[imu]->getPort() << std::endl;
            }
            fileStream << isUsingQuaternions() << '\t';
            fileStream << isUsingAccelerometers() << '\t';
            fileStream << isUsingGyroscopes() << '\t';
            fileStream << isUsingMagnetometers() << '\t';
            fileStream << isUsingAnalogue() << std::endl;
            fileStream << getQuaternionHistoryLength() << '\t';
            fileStream << getAccelerometerHistoryLength() << '\t';
            fileStream << getGyroscopeHistoryLength() << '\t';
            fileStream << getMagnetometerHistoryLength() << '\t';
            fileStream << getAnalogueHistoryLength() << '\t';
            fileStream << getIMUSampleFrequency() << std::endl;
            fileStream << masterAccelerometerFilter << std::endl;
            fileStream << masterGyroscopeFilter << std::endl;
            fileStream << masterMagnetometerFilter << std::endl;
            fileStream << masterAnalogueFilter << std::endl;
            return true;
        }
        return false;
    }

    bool IMUDataCollector::loadIMUSettings(const wxString& filename)
    {
        std::ifstream fileStream(filename.c_str(), std::ifstream::in);
        if (!fileStream.is_open()) return false;
        Str line;
        if (!getNext(fileStream, line)) return false;
        unsigned int version = 0;
        if (line == "ISv1") version = 1;
        else if (line == "ISv2") version = 2;
        else if (line == "ISv3") version = 3;
        else if (line == "ISv4") version = 4;
        if (!version) return false;

        unsigned int imus;
        fileStream >> imus;
        // Move to next line.
        line = getNext(fileStream);
        if (!fileStream.good()) return false;
        unsigned int typeIndex[imus];
        Str portName[imus];
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            fileStream >> typeIndex[imu];
            if(!getNext(fileStream, portName[imu]))return false;
        }
        bool usingQuaternions;
        bool usingAccelerometers;
        bool usingGyroscopes;
        bool usingMagnetometers;
        bool usingAnalogue;
        unsigned int historyLength[5];
        unsigned int sampleFrequency;
        MultiFilter acclerometerFilter(AXES);
        MultiFilter gyroscopeFilter(AXES);
        MultiFilter magnetometerFilter(AXES);
        MultiFilter analogueFilter(CHANNELS);
        fileStream >> usingQuaternions;
        fileStream >> usingAccelerometers;
        fileStream >> usingGyroscopes;
        fileStream >> usingMagnetometers;
        fileStream >> usingAnalogue;
        if (version >= 4)
        {
            for (unsigned int i = 0; i < 5; ++i)
            fileStream >> historyLength[i];
            fileStream >> sampleFrequency;
        }
        else
        {
            for (unsigned int i = 0; i < 5; ++i)
            historyLength[i] = 1;
            sampleFrequency = 1000;
        }
        if (version >= 3)
        {
            fileStream >> acclerometerFilter;
            fileStream >> gyroscopeFilter;
            fileStream >> magnetometerFilter;
        }
        if (version >= 2)
        {
            fileStream >> analogueFilter;
        }
        if (fileStream.good())
        {
            if (acclerometerFilter.getChannels() == AXES)
                masterAccelerometerFilter = acclerometerFilter;
            if (gyroscopeFilter.getChannels() == AXES)
                masterGyroscopeFilter = gyroscopeFilter;
            if (magnetometerFilter.getChannels() == AXES)
                masterMagnetometerFilter = magnetometerFilter;
            if (analogueFilter.getChannels() == CHANNELS)
                masterAnalogueFilter = analogueFilter;
            setQuaternionHistoryLength(historyLength[0]);
            setAccelerometerHistoryLength(historyLength[1]);
            setGyroscopeHistoryLength(historyLength[2]);
            setMagnetometerHistoryLength(historyLength[3]);
            setAnalogueHistoryLength(historyLength[4]);
            setIMUSampleFrequency(sampleFrequency);
            setIMUs(imus);
            for (unsigned int imu = 0; imu < imus; ++imu)
            {
                setIMUType(typeIndex[imu], imu);
                imuapi[imu]->setPort(portName[imu]);
            }
            setUsingQuaternions(usingQuaternions);
            setUsingAccelerometers(usingAccelerometers);
            setUsingGyroscopes(usingGyroscopes);
            setUsingMagnetometers(usingMagnetometers);
            setUsingAnalogue(usingAnalogue);
            return true;
        }
        return false;
    }

    Serial* IMUDataCollector::getSerial(unsigned int imu)
    {
        return imuapi[imu]->getSerial();
    }

    FileSerial* IMUDataCollector::setFileSerial(FileSerial* newFileSerial,
            unsigned int imu)
    {
        if (imu > imus) return 0;
        nu::FileAPI* fileAPI = dynamic_cast<nu::FileAPI*>(imuapi[imu]);
        if (!fileAPI) return 0;
        return fileAPI->setFileSerial(newFileSerial);
    }

    bool IMUDataCollector::resetFileSerialTimer(unsigned int imu)
    {
        if (imu > imus) return false;
        nu::FileAPI* fileAPI = dynamic_cast<nu::FileAPI*>(imuapi[imu]);
        if (!fileAPI) return false;
        fileAPI->resetFileSerialTimer();
        return true;
    }

    IMULogger* IMUDataCollector::getLogger() const
    {
        return logger;
    }

    void IMUDataCollector::setLogger(IMULogger* logger)
    {
        this->logger = logger;
    }

    void IMUDataCollector::displayPackets(bool display)
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            listener[imu]->displayPackets(display);
        }
    }

    bool IMUDataCollector::isUsingQuaternions()
    {
        wxMutexLocker valueLock(valueAccess);
        return usingQuaternions;
    }

    void IMUDataCollector::setUsingQuaternions(const bool usingQuaternions)
    {
        wxMutexLocker valueLock(valueAccess);
        this->usingQuaternions = usingQuaternions;
    }

    bool IMUDataCollector::isUsingAccelerometers()
    {
        wxMutexLocker valueLock(valueAccess);
        return usingAccelerometers;
    }

    void IMUDataCollector::setUsingAccelerometers(const bool usingAccelerometers)
    {
        wxMutexLocker valueLock(valueAccess);
        this->usingAccelerometers = usingAccelerometers;
    }

    bool IMUDataCollector::isUsingGyroscopes()
    {
        wxMutexLocker valueLock(valueAccess);
        return usingGyroscopes;
    }

    void IMUDataCollector::setUsingGyroscopes(const bool usingGyroscopes)
    {
        wxMutexLocker valueLock(valueAccess);
        this->usingGyroscopes = usingGyroscopes;
    }

    bool IMUDataCollector::isUsingMagnetometers()
    {
        wxMutexLocker valueLock(valueAccess);
        return usingMagnetometers;
    }

    void IMUDataCollector::setUsingMagnetometers(const bool usingMagnetometers)
    {
        wxMutexLocker valueLock(valueAccess);
        this->usingMagnetometers = usingMagnetometers;
    }

    bool IMUDataCollector::isUsingAnalogue()
    {
        wxMutexLocker valueLock(valueAccess);
        return usingAnalogue;
    }

    void IMUDataCollector::setUsingAnalogue(const bool usingAnalogue)
    {
        wxMutexLocker valueLock(valueAccess);
        this->usingAnalogue = usingAnalogue;
    }

    unsigned int IMUDataCollector::getQuaternionHistoryLength() const
    {
        return historyLength[0];
    }

    void IMUDataCollector::setQuaternionHistoryLength(
            const unsigned int historyLength)
    {
        wxMutexLocker valueLock(valueAccess);
        this->historyLength[0] = historyLength;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            quaternionHistory[imu] = TemporalData(4, sampleFrequency,
                    historyLength);
        }
    }

    unsigned int IMUDataCollector::getAccelerometerHistoryLength() const
    {
        return historyLength[1];
    }

    void IMUDataCollector::setAccelerometerHistoryLength(
            const unsigned int historyLength)
    {
        wxMutexLocker valueLock(valueAccess);
        this->historyLength[1] = historyLength;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            accelerometerHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength);
        }
    }

    unsigned int IMUDataCollector::getGyroscopeHistoryLength() const
    {
        return historyLength[2];
    }

    void IMUDataCollector::setGyroscopeHistoryLength(
            const unsigned int historyLength)
    {
        wxMutexLocker valueLock(valueAccess);
        this->historyLength[2] = historyLength;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            gyroscopeHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength);
        }
    }

    unsigned int IMUDataCollector::getMagnetometerHistoryLength() const
    {
        return historyLength[3];
    }

    void IMUDataCollector::setMagnetometerHistoryLength(
            const unsigned int historyLength)
    {
        wxMutexLocker valueLock(valueAccess);
        this->historyLength[3] = historyLength;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            magnetometerHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength);
        }
    }

    unsigned int IMUDataCollector::getAnalogueHistoryLength() const
    {
        return historyLength[4];
    }

    void IMUDataCollector::setAnalogueHistoryLength(
            const unsigned int historyLength)
    {
        wxMutexLocker valueLock(valueAccess);
        this->historyLength[4] = historyLength;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            analogueHistory[imu] = TemporalData(CHANNELS, sampleFrequency,
                    historyLength);
        }
    }

    unsigned int IMUDataCollector::getHistoryLength() const
    {
        unsigned int historyLength = 0;
        if (getAnalogueHistoryLength() > historyLength)
            historyLength = getAnalogueHistoryLength();
        if (getAccelerometerHistoryLength() > historyLength)
            historyLength = getAccelerometerHistoryLength();
        if (getGyroscopeHistoryLength() > historyLength)
            historyLength = getGyroscopeHistoryLength();
        if (getMagnetometerHistoryLength() > historyLength)
            historyLength = getMagnetometerHistoryLength();
        if (getQuaternionHistoryLength() > historyLength)
            historyLength = getQuaternionHistoryLength();
        historyLength = historyLength * getIMUSampleFrequency() / 1000;
        return historyLength;
    }

    unsigned int IMUDataCollector::getIMUSampleFrequency() const
    {
        return sampleFrequency;
    }

    void IMUDataCollector::setIMUSampleFrequency(
            const unsigned int sampleFrequency)
    {
        wxMutexLocker valueLock(valueAccess);
        this->sampleFrequency = sampleFrequency;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            quaternionHistory[imu] = TemporalData(4, sampleFrequency,
                    historyLength[0]);
            accelerometerHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength[1]);
            gyroscopeHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength[2]);
            magnetometerHistory[imu] = TemporalData(AXES, sampleFrequency,
                    historyLength[3]);
            analogueHistory[imu] = TemporalData(CHANNELS, sampleFrequency,
                    historyLength[4]);
        }
    }

    void IMUDataCollector::quaternionReceived(const Vector& quaternion,
            const unsigned int imu, bool realTime)
    {
        if (usingQuaternions && imu < imus)
        {
            wxMutexLocker valueLock(valueAccess);
            if (realTime)
            {
                quaternionHistory[imu].addData(quaternion);
            }
            else
            {
                quaternionHistory[imu].addData(quaternion,
                        1000 / sampleFrequency);
            }
        }
        if (logger) logger->newQuaternion(quaternion, imu);
    }

    void IMUDataCollector::accelerometerDataReceived(const Vector& accelerometer,
            const unsigned int imu, bool realTime)
    {
        if (usingAccelerometers && imu < imus)
        {
            wxMutexLocker valueLock(valueAccess);
            Vector filteredAccelerometer =
                    accelerometerFilter[imu].filterAll(accelerometer);
            if (realTime)
            {
                accelerometerHistory[imu].addData(filteredAccelerometer);
            }
            else
            {
                accelerometerHistory[imu].addData(filteredAccelerometer,
                        1000 / sampleFrequency);
            }
        }
        if (logger) logger->newAccelerometerData(accelerometer, imu);
    }

    void IMUDataCollector::gyroscopeDataReceived(const Vector& gyroscope,
            const unsigned int imu, bool realTime)
    {
        if (usingGyroscopes && imu < imus)
        {
            wxMutexLocker valueLock(valueAccess);
            Vector filteredGyroscpe =
                    gyroscopeFilter[imu].filterAll(gyroscope);
            if (realTime)
            {
                gyroscopeHistory[imu].addData(filteredGyroscpe);
            }
            else
            {
                gyroscopeHistory[imu].addData(filteredGyroscpe,
                        1000 / sampleFrequency);
            }
        }
        if (logger) logger->newGyroscopeData(gyroscope, imu);
    }

    void IMUDataCollector::magnetometerDataReceived(const Vector& magnetometer,
            const unsigned int imu, bool realTime)
    {
        if (usingMagnetometers && imu < imus)
        {
            wxMutexLocker valueLock(valueAccess);
            Vector filteredMagnetometer =
                    magnetometerFilter[imu].filterAll(magnetometer);
            if (realTime)
            {
                magnetometerHistory[imu].addData(filteredMagnetometer);
            }
            else
            {
                magnetometerHistory[imu].addData(filteredMagnetometer,
                        1000 / sampleFrequency);
            }
        }
        if (logger) logger->newMagnetometerData(magnetometer, imu);
    }

    void IMUDataCollector::analogueDataReceived(const Vector& analogue,
            const unsigned int imu, bool realTime)
    {
        if (usingAnalogue && imu < imus)
        {
            wxMutexLocker valueLock(valueAccess);
            Vector filteredAnalogue =
                    analogueFilter[imu].filterAll(analogue);
            if (realTime)
            {
                analogueHistory[imu].addData(filteredAnalogue);
            }
            else
            {
                analogueHistory[imu].addData(filteredAnalogue,
                        1000 / sampleFrequency);
            }

        }
        if (logger) logger->newAnalogueData(analogue, imu);
    }

    void IMUDataCollector::registerDataReceived(const Vector& registers,
            const unsigned int imu)
    {
        if (imu < imus)
        {
            registerData = Data(DataType::RegisterData, registers);
            waitingForRegisterData = false;
        }
    }

    void IMUDataCollector::signalDataReceived(const unsigned int signalData,
            const unsigned int imu, bool realTime)
    {
        if (imu < imus) signal[imu] = signalData;
        if (logger) logger->newSignalData(signalData, imu);
    }

    void IMUDataCollector::xIMUTare()
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            xIMU::API* xIMUAPI = dynamic_cast<xIMU::API*>(imuapi[imu]);
            if (xIMUAPI)
            {
                xIMUAPI->sendCommand(xIMU::CommandCode::AlgorithmTare);
            }
        }
    }

    void IMUDataCollector::nuIMUDataStart()
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            nu::API* nuAPI = dynamic_cast<nu::API*>(imuapi[imu]);
            if (nuAPI)
            {
                nuAPI->sendCommand("M4dg");
            }
        }
    }

    void IMUDataCollector::nuQuaternionStart()
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            nu::API* nuAPI = dynamic_cast<nu::API*>(imuapi[imu]);
            if (nuAPI)
            {
                nuAPI->sendCommand("M7dg");
            }
        }
    }

    void IMUDataCollector::nuStop()
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            nu::API* nuAPI = dynamic_cast<nu::API*>(imuapi[imu]);
            if (nuAPI)
            {
                nuAPI->sendCommand("M0dg");
            }
        }
    }

    MultiFilter* IMUDataCollector::getMasterAccelerometerFilter()
    {
        return &masterAccelerometerFilter;
    }

    MultiFilter* IMUDataCollector::getMasterGyroscopeFilter()
    {
        return &masterGyroscopeFilter;
    }

    MultiFilter* IMUDataCollector::getMasterMagnetometerFilter()
    {
        return &masterMagnetometerFilter;
    }

    MultiFilter* IMUDataCollector::getMasterAnalogueFilter()
    {
        return &masterAnalogueFilter;
    }

    void IMUDataCollector::replaceFilters()
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            accelerometerFilter[imu] = masterAccelerometerFilter;
            gyroscopeFilter[imu] = masterGyroscopeFilter;
            magnetometerFilter[imu] = masterMagnetometerFilter;
            analogueFilter[imu] = masterAnalogueFilter;
        }
    }

    const unsigned int IMUDataCollector::imuTypes = 4;
    const wxString IMUDataCollector::imuTypeNames[] = {
        wxT("xIMU"),
        wxT("nu"),
        wxT("nuDataFile"),
        wxT("nuRawFile")
        };
}
