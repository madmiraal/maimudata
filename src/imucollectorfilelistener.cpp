// (c) 2018: Marcel Admiraal

#include "imucollectorfilelistener.h"

#include "imudatacollector.h"

namespace ma
{
    IMUCollectorFileListener::IMUCollectorFileListener(
            IMUDataCollector* collector, const unsigned int imuIndex) :
            collector(collector), imuIndex(imuIndex)
    {
    }

    IMUCollectorFileListener::~IMUCollectorFileListener()
    {
    }

    void IMUCollectorFileListener::onSignalDataReceived(const Data& data)
    {
        collector->signalDataReceived(data.getVectorData()[0], imuIndex);
    }
}
