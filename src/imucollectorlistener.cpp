// (c) 2018: Marcel Admiraal

#include "imucollectorlistener.h"

#include "imudatacollector.h"

namespace ma
{
    IMUCollectorListener::IMUCollectorListener(IMUDataCollector* collector,
            const unsigned int imuIndex) :
            collector(collector), imuIndex(imuIndex), displayingPackets(false)
    {
    }

    IMUCollectorListener::~IMUCollectorListener()
    {
    }

    void IMUCollectorListener::onAccelerometerDataReceived(const Data& data)
    {
        collector->accelerometerDataReceived(data.getVectorData(), imuIndex);
        if (displayingPackets)
            std::cout << "Accelerometer data received: "
                    << data.getVectorData() << std::endl;
    }

    void IMUCollectorListener::onGyroscopeDataReceived(const Data& data)
    {
        collector->gyroscopeDataReceived(data.getVectorData(), imuIndex);
        if (displayingPackets)
            std::cout << "Gyroscope data received: "
                    << data.getVectorData() << std::endl;
    }

    void IMUCollectorListener::onMagnetometerDataReceived(const Data& data)
    {
        collector->magnetometerDataReceived(data.getVectorData(), imuIndex);
        if (displayingPackets)
            std::cout << "Magnetometer data received: "
                    << data.getVectorData() << std::endl;
    }

    void IMUCollectorListener::onQuaternionReceived(const Data& data)
    {
        collector->quaternionReceived(data.getVectorData(), imuIndex);
        if (displayingPackets)
            std::cout << "Quaternion data received: "
                    << data.getVectorData() << std::endl;
    }

    void IMUCollectorListener::onAnalogueDataReceived(const Data& data)
    {
        collector->analogueDataReceived(data.getVectorData(), imuIndex);
        if (displayingPackets)
            std::cout << "Analogue data received: "
                    << data.getVectorData() << std::endl;
    }

    void IMUCollectorListener::onRegisterDataReceived(const Data& data)
    {
        collector->registerDataReceived(data.getVectorData(), imuIndex);
        if (displayingPackets)
            std::cout << "Register data received: "
                    << data.getVectorData() << std::endl;
    }

    void IMUCollectorListener::displayPackets(bool display)
    {
        displayingPackets = display;
    }
}
