// (c) 2016: Marcel Admiraal

#include "imulogplayback.h"

#include "strstream.h"

namespace ma
{
    IMULogPlayback::IMULogPlayback(IMUDataCollector* dataCollector) :
        LogPlayback(), dataCollector(dataCollector)
    {
    }

    IMULogPlayback::~IMULogPlayback()
    {
    }

    bool IMULogPlayback::processEntry(const Str& entry)
    {
        if (!ma::LogPlayback::processEntry(entry))
        {
            if (entry.left(21) == "Quaternion Received: ")
                newQuaternion(entry.trimLeft(21));
            else if (entry.left(29) == "Accelerometer Data Received: ")
                newAccelerometerData(entry.trimLeft(29));
            else if (entry.left(25) == "Gyroscope Data Received: ")
                newGyroscopeData(entry.trimLeft(25));
            else if (entry.left(28) == "Magnetometer Data Received: ")
                newGyroscopeData(entry.trimLeft(28));
            else if (entry.left(24) == "Analogue Data Received: ")
                newAnalogueData(entry.trimLeft(24));
            else if (entry.left(22) == "Signal Data Received: ")
                newSignalData(entry.trimLeft(22));
            else
                return false;
        }
        return true;
    }

    void IMULogPlayback::newQuaternion(const Str& quaternionString)
    {
        StrStream dataStream(quaternionString);
        Vector data;
        unsigned int imu = (unsigned int)-1;
        dataStream >> data;
        dataStream >> imu;
        if (data.size() == 4 && imu != (unsigned int)-1)
        {
            dataCollector->quaternionReceived(data, imu, realTime);
        }
    }

    void IMULogPlayback::newAccelerometerData(const Str& accelerometerDataString)
    {
        StrStream dataStream(accelerometerDataString);
        Vector data;
        unsigned int imu = (unsigned int)-1;
        dataStream >> data;
        dataStream >> imu;
        if (data.size() == 3 && imu != (unsigned int)-1)
        {
            dataCollector->accelerometerDataReceived(data, imu, realTime);
        }
    }

    void IMULogPlayback::newGyroscopeData(const Str& gyroscopeDataString)
    {
        StrStream dataStream(gyroscopeDataString);
        Vector data;
        unsigned int imu = (unsigned int)-1;
        dataStream >> data;
        dataStream >> imu;
        if (data.size() == 3 && imu != (unsigned int)-1)
        {
            dataCollector->gyroscopeDataReceived(data, imu, realTime);
        }
    }

    void IMULogPlayback::newMagnetometerData(const Str& magnetometerDataString)
    {
        StrStream dataStream(magnetometerDataString);
        Vector data;
        unsigned int imu = (unsigned int)-1;
        dataStream >> data;
        dataStream >> imu;
        if (data.size() == 3 && imu != (unsigned int)-1)
        {
            dataCollector->magnetometerDataReceived(data, imu, realTime);
        }
    }

    void IMULogPlayback::newAnalogueData(const Str& analogueDataString)
    {
        StrStream dataStream(analogueDataString);
        Vector data;
        unsigned int imu = (unsigned int)-1;
        dataStream >> data;
        dataStream >> imu;
        if (data.size() == 8 && imu != (unsigned int)-1)
        {
            dataCollector->analogueDataReceived(data, imu, realTime);
        }
    }

    void IMULogPlayback::newSignalData(const Str& signalDataString)
    {
        StrStream dataStream(signalDataString);
        unsigned int data = (unsigned int)-1;
        unsigned int imu = (unsigned int)-1;
        dataStream >> data;
        dataStream >> imu;
        if (data != (unsigned int)-1 && imu != (unsigned int)-1)
        {
            dataCollector->signalDataReceived(data, imu, realTime);
        }
    }
}
