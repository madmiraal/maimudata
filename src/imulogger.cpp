// (c) 2016: Marcel Admiraal

#include "imulogger.h"

#include "strstream.h"

namespace ma
{
    IMULogger::IMULogger() :
        Logger()
    {
    }

    IMULogger::~IMULogger()
    {
    }

    void IMULogger::newQuaternion(const Vector& quaternion,
            const unsigned int imu, const unsigned int time)
    {
        StrStream logLine("Quaternion Received: ");
        logLine << quaternion;
        logLine << " from IMU " << imu;
        log(logLine.toStr(), time);
    }

    void IMULogger::newAccelerometerData(const Vector& accelerometer,
            const unsigned int imu, const unsigned int time)
    {
        StrStream logLine("Accelerometer Data Received: ");
        logLine << accelerometer;
        logLine << " from IMU " << imu;
        log(logLine.toStr(), time);
    }

    void IMULogger::newGyroscopeData(const Vector& gyroscope,
            const unsigned int imu, const unsigned int time)
    {
        StrStream logLine("Gyroscope Data Received: ");
        logLine << gyroscope;
        logLine << " from IMU " << imu;
        log(logLine.toStr(), time);
    }

    void IMULogger::newMagnetometerData(const Vector& magnetometer,
            const unsigned int imu, const unsigned int time)
    {
        StrStream logLine("Magnetometer Data Received: ");
        logLine << magnetometer;
        logLine << " from IMU " << imu;
        log(logLine.toStr(), time);
    }

    void IMULogger::newAnalogueData(const Vector& analogue,
            const unsigned int imu, const unsigned int time)
    {
        StrStream logLine("Analogue Data Received: ");
        logLine << analogue;
        logLine << " from IMU " << imu;
        log(logLine.toStr(), time);
    }

    void IMULogger::newSignalData(const unsigned int signalData,
            const unsigned int imu, const unsigned int time)
    {
        StrStream logLine("Signal Data Received: ");
        logLine << signalData;
        logLine << " from IMU " << imu;
        log(logLine.toStr(), time);
    }
}
