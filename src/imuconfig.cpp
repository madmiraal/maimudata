// (c) 2016 - 2017: Marcel Admiraal

#include "imuconfig.h"

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/button.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/combobox.h>
#include <wx/choicdlg.h>
#include <wx/spinctrl.h>

#include "serialpanel.h"
#include "filterfactory.h"
#include "imudata.h"
#include "nuconfig.h"

namespace ma
{
    // Control IDs.
    enum
    {
        ID_QUATERNION_CHECKBOX,
        ID_ACCELEROMETER_CHECKBOX,
        ID_GYROSCOPE_CHECKBOX,
        ID_MAGNETOMETER_CHECKBOX,
        ID_ANALOGUE_CHECKBOX,
        ID_QUATERNION_SPINNER,
        ID_ACCELEROMETER_SPINNER,
        ID_GYROSCOPE_SPINNER,
        ID_MAGNETOMETER_SPINNER,
        ID_ANALOGUE_SPINNER,
        ID_CONFIGURE_BUTTON_START
    };

    IMUConfig::IMUConfig(wxWindow* parent, IMUDataCollector* dataCollector) :
        wxDialog(parent, wxID_ANY, wxT("Configure IMUs")),
        dataCollector(dataCollector)
    {
        unsigned int imus = dataCollector->getIMUs();
        wxString imusT; imusT << imus;

        // Create Dialog
        wxBoxSizer* configIMUSizer = new wxBoxSizer(wxVERTICAL);

        // Number of IMUs
        wxBoxSizer* numIMUsSizer = new wxBoxSizer(wxHORIZONTAL);
        numIMUsSizer->Add(
                new wxStaticText(this, wxID_ANY, wxT("Number of IMUs: ")),
                0, wxALIGN_LEFT | wxALL, 5);
        numIMUsSpinner = new wxSpinCtrl(this, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize,
                wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 8, imus);
        numIMUsSpinner->Bind(wxEVT_SPINCTRL, &IMUConfig::setNumIMUs, this);
        numIMUsSizer->Add(numIMUsSpinner);
        configIMUSizer->Add(numIMUsSizer);

        // Configure IMUs.
        imuListSizer = new wxStaticBoxSizer(
                new wxStaticBox(this, wxID_ANY, wxT("IMU Port Configuration")),
                wxVERTICAL);
        configIMUSizer->Add(imuListSizer, 0, wxEXPAND);
        poplulateIMUListSizer();

        // Set signals to use.
        wxStaticBoxSizer* imuSignalSizer = new wxStaticBoxSizer(
                new wxStaticBox(this, wxID_ANY, wxT("IMU Signals to Use")),
                wxHORIZONTAL);
        // Quaternions
        quaternionCheckBox = new wxCheckBox(this, ID_QUATERNION_CHECKBOX,
                wxT("Quaternions"));
        quaternionCheckBox->Bind(wxEVT_CHECKBOX,
                &IMUConfig::imuSignalUsedChanged, this);
        imuSignalSizer->Add(quaternionCheckBox);
        // Accelerometers
        accelerometerCheckBox = new wxCheckBox(this, ID_ACCELEROMETER_CHECKBOX,
                wxT("Accelerometers"));
        accelerometerCheckBox->Bind(wxEVT_CHECKBOX,
                &IMUConfig::imuSignalUsedChanged, this);
        imuSignalSizer->Add(accelerometerCheckBox);
        // Gyroscopes
        gyroscopeCheckBox = new wxCheckBox(this, ID_GYROSCOPE_CHECKBOX,
                wxT("Gyroscopes"));
        gyroscopeCheckBox->Bind(wxEVT_CHECKBOX,
                &IMUConfig::imuSignalUsedChanged, this);
        imuSignalSizer->Add(gyroscopeCheckBox);
        // Magnetometers
        magnetometerCheckBox = new wxCheckBox(this, ID_MAGNETOMETER_CHECKBOX,
                wxT("Magnetometers"));
        magnetometerCheckBox->Bind(wxEVT_CHECKBOX,
                &IMUConfig::imuSignalUsedChanged, this);
        imuSignalSizer->Add(magnetometerCheckBox);
        // Analogue data
        analogueCheckBox = new wxCheckBox(this, ID_ANALOGUE_CHECKBOX,
                wxT("Analogue"));
        analogueCheckBox->Bind(wxEVT_CHECKBOX,
                &IMUConfig::imuSignalUsedChanged, this);
        imuSignalSizer->Add(analogueCheckBox);
        configIMUSizer->Add(imuSignalSizer, 0, wxEXPAND);
        setIMUCheckboxes();

        // Set signal history length.
        wxStaticBoxSizer* imuHistorySizer = new wxStaticBoxSizer(
                new wxStaticBox(this, wxID_ANY, wxT("IMU History Length (ms)")),
                wxHORIZONTAL);
        // Quaternions
        wxBoxSizer* imuItemHistorySizer = new wxBoxSizer(wxVERTICAL);
        imuItemHistorySizer->Add(
                new wxStaticText(this, wxID_ANY, wxT(" Quaternions:")),
                0, wxALIGN_LEFT);
        quaternionHistorySpinner = new wxSpinCtrl(this,
                ID_QUATERNION_SPINNER, wxEmptyString, wxDefaultPosition,
                wxSize(100, 25), wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 1000,
                dataCollector->getQuaternionHistoryLength());
        Bind(wxEVT_SPINCTRL, &IMUConfig::imuHistoryLengthChanged, this,
                ID_QUATERNION_SPINNER);
        imuItemHistorySizer->Add(quaternionHistorySpinner);
        imuHistorySizer->Add(imuItemHistorySizer);
        // Accelerometers
        imuItemHistorySizer = new wxBoxSizer(wxVERTICAL);
        imuItemHistorySizer->Add(
                new wxStaticText(this, wxID_ANY, wxT(" Accelerometers:")),
                0, wxALIGN_LEFT);
        accelerometerHistorySpinner = new wxSpinCtrl(this,
                ID_ACCELEROMETER_SPINNER, wxEmptyString, wxDefaultPosition,
                wxSize(100, 25), wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 1000,
                dataCollector->getAccelerometerHistoryLength());
        Bind(wxEVT_SPINCTRL, &IMUConfig::imuHistoryLengthChanged, this,
                ID_ACCELEROMETER_SPINNER);
        imuItemHistorySizer->Add(accelerometerHistorySpinner);
        imuHistorySizer->Add(imuItemHistorySizer);
        // Gyroscopes
        imuItemHistorySizer = new wxBoxSizer(wxVERTICAL);
        imuItemHistorySizer->Add(
                new wxStaticText(this, wxID_ANY, wxT(" Gyroscopes:")),
                0, wxALIGN_LEFT);
        gyroscopeHistorySpinner = new wxSpinCtrl(this,
                ID_GYROSCOPE_SPINNER, wxEmptyString, wxDefaultPosition,
                wxSize(100, 25), wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 1000,
                dataCollector->getGyroscopeHistoryLength());
        Bind(wxEVT_SPINCTRL, &IMUConfig::imuHistoryLengthChanged, this,
                ID_GYROSCOPE_SPINNER);
        imuItemHistorySizer->Add(gyroscopeHistorySpinner);
        imuHistorySizer->Add(imuItemHistorySizer);
        // Magnetometers
        imuItemHistorySizer = new wxBoxSizer(wxVERTICAL);
        imuItemHistorySizer->Add(
                new wxStaticText(this, wxID_ANY, wxT(" Magnetometers:")),
                0, wxALIGN_LEFT);
        magnetometerHistorySpinner = new wxSpinCtrl(this,
                ID_MAGNETOMETER_SPINNER, wxEmptyString, wxDefaultPosition,
                wxSize(100, 25), wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 1000,
                dataCollector->getMagnetometerHistoryLength());
        Bind(wxEVT_SPINCTRL, &IMUConfig::imuHistoryLengthChanged, this,
                ID_MAGNETOMETER_SPINNER);
        imuItemHistorySizer->Add(magnetometerHistorySpinner);
        imuHistorySizer->Add(imuItemHistorySizer);
        // Analogue data
        imuItemHistorySizer = new wxBoxSizer(wxVERTICAL);
        imuItemHistorySizer->Add(
                new wxStaticText(this, wxID_ANY, wxT(" Analogue:")), 0,
                wxALIGN_LEFT);
        analogueHistorySpinner = new wxSpinCtrl(this,
                ID_ANALOGUE_SPINNER, wxEmptyString, wxDefaultPosition,
                wxSize(100, 25), wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 1000,
                dataCollector->getAnalogueHistoryLength());
        Bind(wxEVT_SPINCTRL, &IMUConfig::imuHistoryLengthChanged, this,
                ID_ANALOGUE_SPINNER);
        imuItemHistorySizer->Add(analogueHistorySpinner);
        imuHistorySizer->Add(imuItemHistorySizer);
        configIMUSizer->Add(imuHistorySizer, 0, wxEXPAND);

        // Expected IMU Sample Frequency
        wxBoxSizer* imuSampleFrequencySizer = new wxBoxSizer(wxHORIZONTAL);
        imuSampleFrequencySizer->Add(
                new wxStaticText(this, wxID_ANY,
                        wxT("Expected IMU Sample Frequency: ")), 0,
                        wxALIGN_LEFT | wxALL, 5);
        wxSpinCtrl* imuSampleFrequencySpinner =
                new wxSpinCtrl(this, wxID_ANY, wxEmptyString,
                        wxDefaultPosition, wxDefaultSize,
                        wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 1000,
                        dataCollector->getIMUSampleFrequency());
        imuSampleFrequencySpinner->Bind(wxEVT_SPINCTRL,
                &IMUConfig::setSampleFrequency, this);
        imuSampleFrequencySizer->Add(imuSampleFrequencySpinner);
        configIMUSizer->Add(imuSampleFrequencySizer);

        // Filters.
        wxStaticBoxSizer* filterSizer = new wxStaticBoxSizer(
                new wxStaticBox(this, wxID_ANY, wxT("Filters")),
                wxVERTICAL);
        wxString inputOption[4];
        inputOption[0] = "Accelerometer Filters";
        inputOption[1] = "Gyroscope Filters";
        inputOption[2] = "Magnetometer Filters";
        inputOption[3] = "Analogue Filters";
        wxComboBox* inputSelection = new wxComboBox(
                this, wxID_ANY, inputOption[3],
                wxDefaultPosition, wxDefaultSize, 4, inputOption,
                wxCB_READONLY);
        inputSelection->Bind(wxEVT_COMBOBOX, &IMUConfig::inputFilterChanged,
                this);
        filterSizer->Add(inputSelection, 0, wxLEFT, 5);
        multiFilterPanel = new MultiFilterPanel(
                this, dataCollector->getMasterAnalogueFilter());
        filterSizer->Add(multiFilterPanel);
        configIMUSizer->Add(filterSizer, 0, wxEXPAND);

        // IMU Command Buttons
        wxStaticBoxSizer* imuCommandSizer = new wxStaticBoxSizer(
                new wxStaticBox(this, wxID_ANY, wxT("IMU Commands")),
                wxHORIZONTAL);
        wxButton* xIMUTareButton = new wxButton(this, wxID_ANY,
                wxT("xIMU Tare"));
        wxButton* nuIMUDataStartButton = new wxButton(this, wxID_ANY,
                wxT("nu IMU Data Start"));
        wxButton* nuQuaternionStartButton = new wxButton(this, wxID_ANY,
                wxT("nu Quaternion Start"));
        wxButton* nuStopButton = new wxButton(this, wxID_ANY,
                wxT("nu Stop"));
        xIMUTareButton->Bind(wxEVT_BUTTON,
                &IMUConfig::xIMUTare, this);
        nuIMUDataStartButton->Bind(wxEVT_BUTTON,
                &IMUConfig::nuIMUDataStart, this);
        nuQuaternionStartButton->Bind(wxEVT_BUTTON,
                &IMUConfig::nuQuaternionStart, this);
        nuStopButton->Bind(wxEVT_BUTTON,
                &IMUConfig::nuStop, this);
        imuCommandSizer->Add(xIMUTareButton, 1, wxALL, 5);
        imuCommandSizer->Add(nuIMUDataStartButton, 1, wxALL, 5);
        imuCommandSizer->Add(nuQuaternionStartButton, 1, wxALL, 5);
        imuCommandSizer->Add(nuStopButton, 1, wxALL, 5);
        configIMUSizer->Add(imuCommandSizer, 0, wxEXPAND);

        // Load and Save Buttons.
        wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);
        wxButton* loadButton = new wxButton(this, wxID_ANY, wxT("Load"));
        wxButton* saveButton = new wxButton(this, wxID_ANY, wxT("Save"));
        wxButton* doneButton = new wxButton(this, wxID_ANY, wxT("Done"));
        loadButton->Bind(wxEVT_BUTTON, &IMUConfig::loadIMUSettings, this);
        saveButton->Bind(wxEVT_BUTTON, &IMUConfig::saveIMUSettings, this);
        doneButton->Bind(wxEVT_BUTTON, &IMUConfig::doneButton, this);
        buttonSizer->AddStretchSpacer(1);
        buttonSizer->Add(loadButton, 0, wxALL, 5);
        buttonSizer->Add(saveButton, 0, wxALL, 5);
        buttonSizer->Add(doneButton, 0, wxALL, 5);
        buttonSizer->AddStretchSpacer(1);
        configIMUSizer->Add(buttonSizer, 0, wxEXPAND);

        SetSizerAndFit(configIMUSizer);
        SetSizeHints(-1, -1);
    }

    IMUConfig::~IMUConfig()
    {
        dataCollector->replaceFilters();
    }

    void IMUConfig::setNumIMUs(wxCommandEvent& event)
    {
        unsigned int imus = event.GetSelection();
        dataCollector->setIMUs(imus);
        poplulateIMUListSizer();
    }

    void IMUConfig::imuTypeChanged(wxCommandEvent& event)
    {
        dataCollector->setIMUType(event.GetSelection(), event.GetId());
        if (!updateSerial(event.GetId())) poplulateIMUListSizer();
    }

    void IMUConfig::configureIMU(wxCommandEvent& event)
    {
        unsigned int imu = event.GetId() - ID_CONFIGURE_BUTTON_START;
        ma::Data* registerData = dataCollector->getRegisterData(imu);
        if (!registerData)
        {
            wxMessageDialog message(this,
                    wxT("Failed to retrieve register data."));
            message.ShowModal();
        }
        else
        {
            nu::RegisterData nuRegisterData(registerData->getVectorData());
            nu::Config configDialog(this, &nuRegisterData);
            int returnCode = configDialog.ShowModal();
            if (returnCode !=  wxID_CANCEL)
            {
                dataCollector->setRegisterData(imu, &nuRegisterData);
                if (returnCode == wxID_APPLY)
                    dataCollector->saveRegistersToFlash(imu);
            }
        }
    }

    void IMUConfig::imuSignalUsedChanged(wxCommandEvent& event)
    {
        wxCheckBox* checkBox = (wxCheckBox*)event.GetEventObject();
        switch (event.GetId())
        {
        case ID_QUATERNION_CHECKBOX:
            dataCollector->setUsingQuaternions(checkBox->GetValue());
            break;
        case ID_ACCELEROMETER_CHECKBOX:
            dataCollector->setUsingAccelerometers(checkBox->GetValue());
            break;
        case ID_GYROSCOPE_CHECKBOX:
            dataCollector->setUsingGyroscopes(checkBox->GetValue());
            break;
        case ID_MAGNETOMETER_CHECKBOX:
            dataCollector->setUsingMagnetometers(checkBox->GetValue());
            break;
        case ID_ANALOGUE_CHECKBOX:
            dataCollector->setUsingAnalogue(checkBox->GetValue());
            break;
        }
    }

    void IMUConfig::imuHistoryLengthChanged(wxCommandEvent& event)
    {
        switch (event.GetId())
        {
        case ID_QUATERNION_SPINNER:
            dataCollector->setQuaternionHistoryLength(event.GetInt());
            break;
        case ID_ACCELEROMETER_SPINNER:
            dataCollector->setAccelerometerHistoryLength(event.GetInt());
            break;
        case ID_GYROSCOPE_SPINNER:
            dataCollector->setGyroscopeHistoryLength(event.GetInt());
            break;
        case ID_MAGNETOMETER_SPINNER:
            dataCollector->setMagnetometerHistoryLength(event.GetInt());
            break;
        case ID_ANALOGUE_SPINNER:
            dataCollector->setAnalogueHistoryLength(event.GetInt());
            break;
        }
    }

    void IMUConfig::setSampleFrequency(wxCommandEvent& event)
    {
        dataCollector->setIMUSampleFrequency(event.GetInt());
    }

    void IMUConfig::inputFilterChanged(wxCommandEvent& event)
    {
        switch (event.GetSelection())
        {
            case 0:
                multiFilterPanel->setMultiFilter(
                        dataCollector->getMasterAccelerometerFilter());
                break;
            case 1:
                multiFilterPanel->setMultiFilter(
                        dataCollector->getMasterGyroscopeFilter());
                break;
            case 2:
                multiFilterPanel->setMultiFilter(
                        dataCollector->getMasterMagnetometerFilter());
                break;
            case 3:
                multiFilterPanel->setMultiFilter(
                        dataCollector->getMasterAnalogueFilter());
                break;
        }
    }

    void IMUConfig::xIMUTare(wxCommandEvent& event)
    {
        dataCollector->xIMUTare();
    }

    void IMUConfig::nuIMUDataStart(wxCommandEvent& event)
    {
        dataCollector->nuIMUDataStart();
    }

    void IMUConfig::nuQuaternionStart(wxCommandEvent& event)
    {
        dataCollector->nuQuaternionStart();
    }

    void IMUConfig::nuStop(wxCommandEvent& event)
    {
        dataCollector->nuStop();
    }

    void IMUConfig::loadIMUSettings(wxCommandEvent& event)
    {
        bool success = false;
        while (!success)
        {
            wxFileDialog openFileDialog(this, wxT("Load IMU Settings"),
                    wxEmptyString, wxT("imusettings.dat"),
                    wxFileSelectorDefaultWildcardStr,
                    wxFD_OPEN|wxFD_FILE_MUST_EXIST);
            if (openFileDialog.ShowModal() == wxID_CANCEL) return;
            success = dataCollector->loadIMUSettings(openFileDialog.GetPath());
            if (!success)
            {
                wxMessageBox(wxT("Load Failed"));
            }
        }
        poplulateIMUListSizer();
        setIMUCheckboxes();
        setIMUHistoryValues();
    }

    void IMUConfig::saveIMUSettings(wxCommandEvent& event)
    {
        bool success = false;
        while (!success)
        {
            wxFileDialog saveFileDialog(this, wxT("Save IMU Settings"),
                    wxEmptyString, wxT("imusettings.dat"),
                    wxFileSelectorDefaultWildcardStr,
                    wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
            if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
            success = dataCollector->saveIMUsettings(saveFileDialog.GetPath());
            if (!success)
            {
                wxMessageBox(wxT("Save Failed"));
            }
        }
    }

    void IMUConfig::doneButton(wxCommandEvent& event)
    {
        Close();
    }

    void IMUConfig::poplulateIMUListSizer()
    {
        unsigned int imus = dataCollector->getIMUs();
        numIMUsSpinner->SetValue(imus);
        imuListSizer->Clear(true);
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            wxBoxSizer* serialPortSizer = new wxBoxSizer(wxHORIZONTAL);
            wxString imuName = wxT("IMU "); imuName << imu; imuName << ": ";
            serialPortSizer->Add(
                    new wxStaticText(this, wxID_ANY, imuName), 0,
                    wxALIGN_LEFT | wxTOP | wxBOTTOM, 10);
            wxComboBox* imuType = new wxComboBox(
                    this, imu, dataCollector->getIMUTypeName(imu),
                    wxDefaultPosition, wxDefaultSize,
                    dataCollector->getIMUTypes(),
                    dataCollector->getIMUTypeNames(), wxCB_READONLY);
            serialPortSizer->Add(imuType, 0, wxTOP | wxBOTTOM, 5);
            imuType->Bind(wxEVT_COMBOBOX, &IMUConfig::imuTypeChanged, this);
            SerialPanel* serialPanel = new SerialPanel(this,
                    dataCollector->getSerial(imu));
            serialPortSizer->Add(serialPanel, 0, wxLEFT | wxRIGHT, 5);
            wxButton* configureButton = new wxButton(this,
                ID_CONFIGURE_BUTTON_START + imu, wxT("Configure"));
            configureButton->Bind(wxEVT_BUTTON, &IMUConfig::configureIMU, this);
            serialPortSizer->Add(configureButton, 0, wxTOP | wxBOTTOM, 5);
            imuListSizer->Add(serialPortSizer);
        }
        Layout();
        Fit();
    }

    bool IMUConfig::updateSerial(const unsigned int imu)
    {
        wxSizerItem* serialPortSizerItem = imuListSizer->GetItem(imu);
        if (serialPortSizerItem == 0) return false;
        wxSizer* serialPortSizer = serialPortSizerItem->GetSizer();
        if (serialPortSizer == 0) return false;
        wxSizerItem* serialPanelItem = serialPortSizer->GetItem(2);
        if (serialPanelItem == 0) return false;
        wxWindow* serialPanelWindow = serialPanelItem->GetWindow();
        if (serialPanelWindow == 0) return false;
        SerialPanel* serialPanel = dynamic_cast<SerialPanel*>(serialPanelWindow);
        if (serialPanel == 0) return false;
        serialPanel->setSerial(dataCollector->getSerial(imu));
        return true;
    }

    void IMUConfig::setIMUCheckboxes()
    {
        quaternionCheckBox->SetValue(dataCollector->isUsingQuaternions());
        accelerometerCheckBox->SetValue(dataCollector->isUsingAccelerometers());
        gyroscopeCheckBox->SetValue(dataCollector->isUsingGyroscopes());
        magnetometerCheckBox->SetValue(dataCollector->isUsingMagnetometers());
        analogueCheckBox->SetValue(dataCollector->isUsingAnalogue());
    }

    void IMUConfig::setIMUHistoryValues()
    {
        quaternionHistorySpinner->SetValue(
                dataCollector->getQuaternionHistoryLength());
        accelerometerHistorySpinner->SetValue(
                dataCollector->getAccelerometerHistoryLength());
        gyroscopeHistorySpinner->SetValue(
                dataCollector->getGyroscopeHistoryLength());
        magnetometerHistorySpinner->SetValue(
                dataCollector->getMagnetometerHistoryLength());
        analogueHistorySpinner->SetValue(
                dataCollector->getAnalogueHistoryLength());
    }
}
