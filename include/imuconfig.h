// wxWidgets Dialog for configuring IMUs whose data will be stored in an IMU
// Data Collector class.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef IMUCONFIG_H
#define IMUCONFIG_H

#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/spinctrl.h>
#include <wx/checkbox.h>
#include <wx/listctrl.h>

#include "imudatacollector.h"
#include "multifilterpanel.h"

namespace ma
{
    class IMUConfig : public wxDialog
    {
    public:
        /**
         * Constructor.
         *
         * @param parent        The window that owns this dialog.
         * @param dataCollector A pointer to the data collector.
         */
        IMUConfig(wxWindow* parent, IMUDataCollector* dataCollector);

        /**
         * Default destructor.
         */
        virtual ~IMUConfig();

    private:
        void setNumIMUs(wxCommandEvent& event);
        void imuTypeChanged(wxCommandEvent& event);
        void configureIMU(wxCommandEvent& event);
        void imuSignalUsedChanged(wxCommandEvent& event);
        void imuHistoryLengthChanged(wxCommandEvent& event);
        void setSampleFrequency(wxCommandEvent& event);
        void inputFilterChanged(wxCommandEvent& event);
        void xIMUTare(wxCommandEvent& event);
        void nuIMUDataStart(wxCommandEvent& event);
        void nuQuaternionStart(wxCommandEvent& event);
        void nuStop(wxCommandEvent& event);
        void loadIMUSettings(wxCommandEvent& event);
        void saveIMUSettings(wxCommandEvent& event);
        void doneButton(wxCommandEvent& event);

        void poplulateIMUListSizer();
        bool updateSerial(const unsigned int imu);
        void setIMUCheckboxes();
        void setIMUHistoryValues();

        IMUDataCollector* dataCollector;
        wxStaticBoxSizer* imuListSizer;
        wxSpinCtrl* numIMUsSpinner;
        wxCheckBox* quaternionCheckBox;
        wxCheckBox* accelerometerCheckBox;
        wxCheckBox* gyroscopeCheckBox;
        wxCheckBox* magnetometerCheckBox;
        wxCheckBox* analogueCheckBox;
        wxSpinCtrl* quaternionHistorySpinner;
        wxSpinCtrl* accelerometerHistorySpinner;
        wxSpinCtrl* gyroscopeHistorySpinner;
        wxSpinCtrl* magnetometerHistorySpinner;
        wxSpinCtrl* analogueHistorySpinner;
        MultiFilterPanel* multiFilterPanel;
    };
}

#endif // IMUCONFIG_H
