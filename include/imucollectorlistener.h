// Used by the IMU Data Collector for receiving data from multiple types of IMU.
//
// (c) 2016: Marcel Admiraal

#ifndef IMUCOLLECTORLISTENER_H
#define IMUCOLLECTORLISTENER_H

#include "imulistener.h"
#include "ximulistener.h"
#include "nulistener.h"

namespace ma
{
    class IMUDataCollector;

    /**
     * Base abstract IMU Collector Listener.
     */
    class IMUCollectorListener : public virtual IMUListener
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~IMUCollectorListener();

        /**
         * Called when accelerometer data received.
         *
         * @param data  The data received from the IMU.
         */
        virtual void onAccelerometerDataReceived(const Data& data);

        /**
         * Called when gyroscope data received.
         *
         * @param data  The data received from the IMU.
         */
        virtual void onGyroscopeDataReceived(const Data& data);

        /**
         * Called when magnetometer data received.
         *
         * @param data  The data received from the IMU.
         */
        virtual void onMagnetometerDataReceived(const Data& data);

        /**
         * Called when quaternion data received.
         *
         * @param data  The data received from the IMU.
         */
        virtual void onQuaternionReceived(const Data& data);

        /**
         * Called when analogue data received.
         *
         * @param data  The data received from the IMU.
         */
        virtual void onAnalogueDataReceived(const Data& data);

        /**
         * Called when register data received from the IMU.
         *
         * @param data  The register data received.
         */
        virtual void onRegisterDataReceived(const Data& data);

        /**
         * Set whether or not to display the packets received.
         *
         * @param display   Whether or not to display the packets received.
         */
        void displayPackets(bool display = true);

    protected:
        /**
         * Constructor.
         * Note: The IMUCollectorListener is a base class for each of the types
         * of IMU Collector Listeners defined below; so it cannot be intialised
         * itself.
         *
         * @param collector A pointer to the data collector.
         * @param imuIndex  An index used to identify the imu that the listener
         *                  is listening to used by the collector.
         */
        IMUCollectorListener(IMUDataCollector* collector,
                const unsigned int imuIndex);

    private:
        IMUDataCollector* collector;
        unsigned int imuIndex;
        bool displayingPackets;
    };

    /**
     * xIMU Collector Listener.
     */
    class CollectorxIMUListener :
        public IMUCollectorListener, public xIMU::Listener
    {
    public:
        /**
         * Constructor.
         *
         * @param collector A pointer to the data collector.
         * @param imuIndex  An index used to identify the imu that the listener
         *                  is listening to used by the collector.
         */
        CollectorxIMUListener(IMUDataCollector* collector,
                const unsigned int imuIndex) :
                IMUCollectorListener(collector, imuIndex) {}

        /**
         * Default destructor.
         */
        virtual ~CollectorxIMUListener() {}
    };

    /**
     * nuIMU Collector Listener.
     */
    class CollectornuListener :
        public IMUCollectorListener, public nu::Listener
    {
    public:
        /**
         * Constructor.
         *
         * @param collector A pointer to the data collector.
         * @param imuIndex  An index used to identify the imu that the listener
         *                  is listening to used by the collector.
         */
        CollectornuListener(IMUDataCollector* collector,
                const unsigned int imuIndex) :
                IMUCollectorListener(collector, imuIndex) {}

        /**
         * Default destructor.
         */
        virtual ~CollectornuListener() {}
    };
}

#endif // IMUCOLLECTORLISTENER_H
