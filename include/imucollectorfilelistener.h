// Used by the IMU Data Collector for receiving signals from multiple types of
// IMU.
//
// (c) 2018: Marcel Admiraal

#ifndef IMUCOLLECTORFILELISTENER_H
#define IMUCOLLECTORFILELISTENER_H

#include "nufilelistener.h"

namespace ma
{
    class IMUDataCollector;

    class IMUCollectorFileListener : public nu::FileListener
    {
    public:
        /**
         * Constructor.
         *
         * @param collector A pointer to the data collector.
         * @param imuIndex  An index used to identify the imu that the listener
         *                  is listening to used by the collector.
         */
        IMUCollectorFileListener(IMUDataCollector* collector,
                const unsigned int imuIndex);

        /**
         * Default destructor.
         */
        virtual ~IMUCollectorFileListener();

        /**
         * Called when signal data received.
         *
         * @param data  Pointer to the register data received.
         */
        virtual void onSignalDataReceived(const Data& data);

    private:
        IMUDataCollector* collector;
        unsigned int imuIndex;
    };
}

#endif // IMUCOLLECTORFILELISTENER_H
