// Logs data received from IMUs.
//
// Extends the MAwxUtils Logger, which logs data to a log file along with a time
// stamp.
//
// Add the log file to the data logger to automatically log all the data
// received from the IMUs irrespective of the data stored by the data collector.
//
// (c) 2016: Marcel Admiraal

#ifndef IMULOGGER_H
#define IMULOGGER_H

#include "logger.h"
#include "vector.h"

namespace ma
{
    class IMULogger : public Logger
    {
    public:
        /**
         * Default constructor.
         */
        IMULogger();

        /**
         * Default destructor.
         */
        virtual ~IMULogger();

        /**
         * New quaternion data received.
         *
         * @param quaternion    The quaternion data.
         * @param imu           The imu that received the data.
         * @param time          The time of the log entry. Default ms since
         *                      start.
         */
        void newQuaternion(const Vector& quaternion,
                const unsigned int imu, const unsigned int time = -1);

        /**
         * New accelerometer data received.
         *
         * @param accelerometer The accelerometer data.
         * @param imu           The imu that received the data.
         * @param time          The time of the log entry. Default ms since
         *                      start.
         */
        void newAccelerometerData(const Vector& accelerometer,
                const unsigned int imu, const unsigned int time = -1);

        /**
         * New gyroscope data received.
         *
         * @param gyroscope     The gyroscope data.
         * @param imu           The imu that received the data.
         * @param time          The time of the log entry. Default ms since
         *                      start.
         */
        void newGyroscopeData(const Vector& gyroscope,
                const unsigned int imu, const unsigned int time = -1);

        /**
         * New magnetometer data received.
         *
         * @param magnetometer  The magnetometer data.
         * @param imu           The imu that received the data.
         * @param time          The time of the log entry. Default ms since
         *                      start.
         */
        void newMagnetometerData(const Vector& magnetometer,
                const unsigned int imu, const unsigned int time = -1);

        /**
         * New analogue data received.
         *
         * @param quaternion    The quaternion data.
         * @param imu           The imu that received the data.
         * @param time          The time of the log entry. Default ms since
         *                      start.
         */
        void newAnalogueData(const Vector& analogue,
                const unsigned int imu, const unsigned int time = -1);

        /**
         * New signal data received.
         *
         * @param signalData    The signal data.
         * @param imu           The imu that received the data.
         * @param time          The time of the log entry. Default ms since
         *                      start.
         */
        void newSignalData(const unsigned int signalData,
                const unsigned int imu, const unsigned int time = -1);
    };
}

#endif // IMULOGGER_H
