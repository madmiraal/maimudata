// Collects and stores data from multiple IMUs.
//
// The Data Collector is best configured using an IMU Config wxDialog.
//
// (c) 2016 - 2019: Marcel Admiraal

#ifndef IMUDATACOLLECTOR_H
#define IMUDATACOLLECTOR_H

#include <wx/string.h>
#include <wx/thread.h>

#include "serial.h"
#include "fileserial.h"
#include "vector.h"
#include "quaternion.h"
#include "temporaldata.h"
#include "imuapi.h"
#include "imucollectorlistener.h"
#include "imucollectorfilelistener.h"
#include "imulogger.h"
#include "multifilter.h"

namespace ma
{
    class IMUDataCollector
    {
    public:
        /**
         * Default constructor.
         */
        IMUDataCollector();

        /**
         * Default destructor.
         */
        virtual ~IMUDataCollector();

        /**
         * Returns a vector containing all the historical data stored.
         *
         * @return A vector containing all the historical data stored.
         */
        Vector getAllValues();

        /**
         * Returns the total number of data points available.
         *
         * @return The total number of data points available.
         */
        unsigned int getAllValuesSize();

        /**
         * Returns the specified imu's most recent quaternion received.
         *
         * @param imu   The imu's quaternion required.
         * @return      The specified imu's most recent quaternion received.
         */
        Quaternion getCurrentQuaternion(const unsigned int imu);

        /**
         * Returns a vector containing the specified imu's most recent
         * quaternion values received.
         *
         * @param imu   The imu's quaternion values required.
         * @return      A vector containing the specified imu's most recent
         *              quaternion values received.
         */
        Vector getCurrentQuaternionVector(const unsigned int imu);

        /**
         * Returns a vector containing the specified imu's most recent
         * accelerometer values received.
         *
         * @param imu   The imu's accelerometer values required.
         * @return      A vector containing the specified imu's most recent
         *              accelerometer values received.
         */
        Vector getCurrentAccelerometer(const unsigned int imu);

        /**
         * Returns a vector containing the specified imu's most recent
         * gyroscope values received.
         *
         * @param imu   The imu's gyroscope values required.
         * @return      A vector containing the specified imu's most recent
         *              gyroscope values received.
         */
        Vector getCurrentGyroscope(const unsigned int imu);

        /**
         * Returns a vector containing the specified imu's most recent
         * magnetometer values received.
         *
         * @param imu   The imu's magnetometer values required.
         * @return      A vector containing the specified imu's most recent
         *              magnetometer values received.
         */
        Vector getCurrentMagnetometer(const unsigned int imu);

        /**
         * Returns a vector containing the specified imu's most recent
         * analogue data values received.
         *
         * @param imu   The imu's analogue values required.
         * @return      A vector containing the specified imu's most recent
         *              analogue data values received.
         */
        Vector getCurrentAnalogueData(const unsigned int imu);

        /**
         * Returns the specified imu's most recent signal data received.
         *
         * @param imu   The imu's signal data required.
         * @return      The specified imu's most recent signal data received.
         */
        unsigned int getCurrentSignal(const unsigned int imu);

        /**
         * Attempts to return the specified imu's register data.
         *
         * @param imu   The imu's register data required.
         * @return      The specified imu's register data if received.
         */
        Data* getRegisterData(const unsigned int imu);

        /**
         * Sets the specified imu's register data.
         *
         * @param imu           The imu's register data to set.
         * @param registerData  The new register data.
         */
        void setRegisterData(const unsigned int imu, Data* registerData);

        /**
         * Sends signal to save register data to flash.
         * Returns whether or not the save was successful.
         *
         * @param imu   The imu to send the save signal to.
         * @return      Whether or not the save was successful.
         */
        bool saveRegistersToFlash(const unsigned int imu);

        /**
         * Returns the number of IMUs whose data is currently being collected.
         *
         * @return The number of IMUs whose data is currently being collected.
         */
        unsigned int getIMUs() const;

        /**
         * Sets the number of IMUs to collect data for.
         *
         * @param imus  The number of IMUs to collect data for.
         */
        void setIMUs(const unsigned int imus);

        /**
         * Returns the number of types of IMUs whose data can be collected.
         *
         * @return The number of types of IMUs whose data can be collected.
         */
        const unsigned int getIMUTypes() const;

        /**
         * Returns an array of wxStrings with the names of the types of IMU.
         *
         * @return An array of wxStrings with the names of the types of IMU.
         */
        const wxString* getIMUTypeNames() const;

        /**
         * Returns the type name of the IMU specified.
         *
         * @param imu   The IMU whose type name is required.
         * @return      The type name of the IMU specified.
         */
        wxString getIMUTypeName(const unsigned int imu) const;

        /**
         * Sets the type for the IMU specified.
         *
         * @param imuType   The index of the IMU type.
         * @param imu       The index of the IMU.
         */
        void setIMUType(const unsigned int imuType, const unsigned int imu);

        /**
         * Saves the IMU settings in the file specified.
         *
         * @param filename  The filename of the file.
         * @return          Whether or not the save was successful.
         */
        bool saveIMUsettings(const wxString& filename);

        /**
         * Loads the IMU settings from the file specified.
         *
         * @param filename  The filename of the file.
         * @return          Whether or not the load was successful.
         */
        bool loadIMUSettings(const wxString& filename);

        /**
         * Returns a pointer to the serial port of the specified IMU.
         *
         * @param imu   The index of the IMU whose serial port is required.
         * @return      A pointer the serial port.
         */
        Serial* getSerial(unsigned int imu);

        /**
         * Sets the FileSerial used by the specified IMU.
         * Returns a pointer to the previous FileSerial used by the IMU.
         *
         * @param newFileSerial Pointer to the new FileSerial to use.
         * @param imu           The index of the IMU to change.
         * @return              Pointer to the previous FileSerial used.
         */
        FileSerial* setFileSerial(FileSerial* newFileSerial, unsigned int imu);

        /**
         * Resets the FileSerial timer used by the specified IMU.
         * Returns whether or not successful.
         *
         * @param imu   The index of the IMU to reset.
         * @return      Whether or not successful.
         */
        bool resetFileSerialTimer(unsigned int imu);

        /**
         * Returns a pointer to the current IMU Logger used to log the IMU
         * data received.
         *
         * @return A pointer to the IMU Logger.
         */
        IMULogger* getLogger() const;

        /**
         * Sets the current IMU Logger used to log the IMU data received.
         *
         * @param logger    A pointer to the IMU Logger.
         */
        void setLogger(IMULogger* logger);

        /**
         * Set whether or not to display the packets received.
         *
         * @param display   Whether or not to display the packets received.
         */
        void displayPackets(bool display = true);

        /**
         * Returns whether or not quaternions are being stored.
         *
         * @return Whether or not quaternions are stored.
         */
        bool isUsingQuaternions();

        /**
         * Sets whether or not quaternions are stored.
         *
         * @param usingQuaternions  Whether or not quaternions are stored.
         */
        void setUsingQuaternions(const bool usingQuaternions);

        /**
         * Returns whether or not accelerometer data are stored.
         *
         * @return Whether or not accelerometer data are stored.
         */
        bool isUsingAccelerometers();

        /**
         * Sets whether or not accelerometer data are stored.
         *
         * @param usingQuaternions  Whether or not accelerometer data are stored.
         */
        void setUsingAccelerometers(const bool usingAccelerometers);

        /**
         * Returns whether or not gyroscope data are stored.
         *
         * @return Whether or not gyroscope data are stored.
         */
        bool isUsingGyroscopes();

        /**
         * Sets whether or not gyroscope data are stored.
         *
         * @param usingQuaternions  Whether or not gyroscope data are stored.
         */
        void setUsingGyroscopes(const bool usingGyroscopes);

        /**
         * Returns whether or not magnetometer data are stored.
         *
         * @return Whether or not magnetometer data are stored.
         */
        bool isUsingMagnetometers();

        /**
         * Sets whether or not magnetometer data are stored.
         *
         * @param usingQuaternions  Whether or not magnetometer data are stored.
         */
        void setUsingMagnetometers(const bool usingMagnetometers);

        /**
         * Returns whether or not analogue data are stored.
         *
         * @return Whether or not analogue data are stored.
         */
        bool isUsingAnalogue();

        /**
         * Sets whether or not analogue data are stored.
         *
         * @param usingAnalogue Whether or not analogue data are stored.
         */
        void setUsingAnalogue(const bool usingAnalouge);

        /**
         * Returns the current length of the quaternion history.
         *
         * @return The current length of the quaternion history.
         */
        unsigned int getQuaternionHistoryLength() const;

        /**
         * Sets the quaternion history length.
         *
         * @param historyLength The new quaternion history length.
         */
        void setQuaternionHistoryLength(const unsigned int historyLength);

        /**
         * Returns the current length of the accelerometer history.
         *
         * @return The current length of the accelerometer history.
         */
        unsigned int getAccelerometerHistoryLength() const;

        /**
         * Sets the accelerometer history length.
         *
         * @param historyLength The new accelerometer history length.
         */
        void setAccelerometerHistoryLength(const unsigned int historyLength);

        /**
         * Returns the current length of the gyroscope history.
         *
         * @return The current length of the gyroscope history.
         */
        unsigned int getGyroscopeHistoryLength() const;

        /**
         * Sets the gyroscope history length.
         *
         * @param historyLength The new gyroscope history length.
         */
        void setGyroscopeHistoryLength(const unsigned int historyLength);

        /**
         * Returns the current length of the magnetometer history.
         *
         * @return The current length of the magnetometer history.
         */
        unsigned int getMagnetometerHistoryLength() const;

        /**
         * Sets the magnetometer history length.
         *
         * @param historyLength The new magnetometer history length.
         */
        void setMagnetometerHistoryLength(const unsigned int historyLength);

        /**
         * Returns the current length of the analogue history.
         *
         * @return The current length of the analogue history.
         */
        unsigned int getAnalogueHistoryLength() const;

        /**
         * Sets the analogue history length.
         *
         * @param historyLength The new analogue history length.
         */
        void setAnalogueHistoryLength(const unsigned int historyLength);

        /**
         * Returns the longest history length in ms.
         *
         * @return The longest history length in ms.
         */
        unsigned int getHistoryLength() const;

        /**
         * Returns the current expected sample frequency.
         *
         * @return The current expected sample frequency.
         */
        unsigned int getIMUSampleFrequency() const;

        /**
         * Sets the expected sample frequency.
         *
         * @param sampleFrequency The new sample frequency.
         */
        void setIMUSampleFrequency(const unsigned int sampleFrequency);

        /**
         * Called when new quaternion data is received.
         *
         * @param quaternion    A vector containing the quaternion data.
         * @param imu           The index of the imu that sent the data.
         * @param realTime      Whether or not the data is received in real-time.
         */
        void quaternionReceived(const Vector& quaternion,
                const unsigned int imu, bool realTime = true);

        /**
         * Called when new accelerometer data is received.
         *
         * @param accelerometer A vector containing the accelerometer data.
         * @param imu           The index of the imu that sent the data.
         * @param realTime      Whether or not the data is received in real-time.
         */
        void accelerometerDataReceived(const Vector& accelerometer,
                const unsigned int imu, bool realTime = true);

        /**
         * Called when new gyroscope data is received.
         *
         * @param gyroscope A vector containing the gyroscope data.
         * @param imu       The index of the imu that sent the data.
         * @param realTime  Whether or not the data is received in real-time.
         */
        void gyroscopeDataReceived(const Vector& gyroscope,
                const unsigned int imu, bool realTime = true);

        /**
         * Called when new magnetometer data is received.
         *
         * @param magnetometer  A vector containing the magnetometer data.
         * @param imu           The index of the imu that sent the data.
         * @param realTime      Whether or not the data is received in real-time.
         */
        void magnetometerDataReceived(const Vector& magnetometer,
                const unsigned int imu, bool realTime = true);

        /**
         * Called when new analogue data is received.
         *
         * @param analogue  A vector containing the analogue data.
         * @param imu       The index of the imu that sent the data.
         * @param realTime  Whether or not the data is received in real-time.
         */
        void analogueDataReceived(const Vector& analogue,
                const unsigned int imu, bool realTime = true);

        /**
         * Called when new register data is received.
         *
         * @param registers  A vector containing the register data.
         * @param imu       The index of the imu that sent the data.
         */
        void registerDataReceived(const Vector& registers,
                const unsigned int imu);

        /**
         * Called when new signal data is received.
         *
         * @param signalData    The signal data.
         * @param imu           The index of the imu that sent the data.
         */
        void signalDataReceived(const unsigned int signalData,
                const unsigned int imu, bool realTime = true);

        /**
         * Send Tare command to all xIMUs.
         */
        void xIMUTare();

        /**
         * Send IMU data start command (M4dg) to all nu IMUs.
         */
        void nuIMUDataStart();

        /**
         * Send Quaternion data start command (M7dg) to all nu IMUs.
         */
        void nuQuaternionStart();

        /**
         * Send data stop command (M0dg) to all nu IMUs.
         */
        void nuStop();

        /**
         * Returns a pointer to the master accelerometer filter.
         *
         * @return A pointer to the master accelerometer filter.
         */
        MultiFilter* getMasterAccelerometerFilter();

        /**
         * Returns a pointer to the master gyroscope filter.
         *
         * @return A pointer to the master gyroscope filter.
         */
        MultiFilter* getMasterGyroscopeFilter();

        /**
         * Returns a pointer to the master magnetometer filter.
         *
         * @return A pointer to the master magnetometer filter.
         */
        MultiFilter* getMasterMagnetometerFilter();

        /**
         * Returns a pointer to the master analogue filter.
         *
         * @return A pointer to the master analogue filter.
         */
        MultiFilter* getMasterAnalogueFilter();

        /**
         * Replace all filters with the current master filter.
         */
        void replaceFilters();

    private:
        // Number of IMUs.
        unsigned int imus;
        // IMU type indices.
        unsigned int* imuTypeIndex;
        // Array of IMU API pointers.
        IMUAPI** imuapi;
        // Array of IMU listeners.
        IMUCollectorListener** listener;
        IMUCollectorFileListener** fileListener;
        // Logger
        IMULogger* logger;
        // Register data received.
        Data registerData;
        bool waitingForRegisterData;

        // Controls what values are stored.
        bool usingQuaternions;
        bool usingAccelerometers;
        bool usingGyroscopes;
        bool usingMagnetometers;
        bool usingAnalogue;

        // Stores the values received.
        unsigned int* historyLength;
        unsigned int sampleFrequency;
        TemporalData* quaternionHistory;
        TemporalData* accelerometerHistory;
        TemporalData* gyroscopeHistory;
        TemporalData* magnetometerHistory;
        TemporalData* analogueHistory;
        unsigned int* signal;

        // Filters.
        MultiFilter* accelerometerFilter;
        MultiFilter* gyroscopeFilter;
        MultiFilter* magnetometerFilter;
        MultiFilter* analogueFilter;
        MultiFilter masterAccelerometerFilter;
        MultiFilter masterGyroscopeFilter;
        MultiFilter masterMagnetometerFilter;
        MultiFilter masterAnalogueFilter;

        // Access control.
        wxMutex valueAccess;

        static const unsigned int imuTypes;
        static const wxString imuTypeNames[];
    };
}

#endif // IMUDATACOLLECTOR_H
