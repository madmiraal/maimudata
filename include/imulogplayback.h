// Replays files created by IMU Logger.
//
// (c) 2016: Marcel Admiraal

#ifndef IMULOGPLAYBACK_H
#define IMULOGPLAYBACK_H

#include "logplayback.h"
#include "imudatacollector.h"
#include "str.h"

namespace ma
{
    class IMULogPlayback : public LogPlayback
    {
    public:
        /**
         * Constructor.
         *
         * @param dataCollector Pointer to the dataCollector that will receive
         *                      the imu data entries.
         */
        IMULogPlayback(IMUDataCollector* dataCollector);

        /**
         * Default destructor.
         */
        virtual ~IMULogPlayback();

    protected:
        /**
         * Processes the log entry, and returns whether or not the log entry was
         * processed.
         * Note: When overriding this function, call the parent function first,
         * and only process the entry if the parent function returns false i.e.
         * it didn't process the entry.
         *
         * @param entry     The entry to process.
         * @return          Whether or not the log entry was processed.
         */
        virtual bool processEntry(const Str& entry);

    private:
        void newQuaternion(const Str& quaternionString);
        void newAccelerometerData(const Str& accelerometerDataString);
        void newGyroscopeData(const Str& gyroscopeDataString);
        void newMagnetometerData(const Str& magnetometerDataString);
        void newAnalogueData(const Str& analogueDataString);
        void newSignalData(const Str& signalDataString);

        IMUDataCollector* dataCollector;
    };
}

#endif // IMULOGPLAYBACK_H
