# MAIMUData #

Cross-platform C++ code for working with data from multiple IMUs of different types.

Currently two types of IMU are supported: [x-IMU](http://x-io.co.uk/) and the [nu IMU](https://bitbucket.org/account/user/biomechatronicslab/projects/NUIM).

## IMU Data Collection ##
* ma::IMUDataCollector: Automatically stores the required data received from the IMUs, which can be extracted when required.
* ma::IMUConfig: A wxDialog that facilitates configuring the IMU Data Collector.
* ma::IMULogger: When added to the IMU Data Collector, stores all data received from the IMUs, irrespective of the settings specifying what data is stored.
* ma::IMUPlayback: Replays a logfile created with IMULogger into a IMU Data Collector to enable different data to be stored and processed.

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
* [MAwxUtils](https://bitbucket.org/madmiraal/mawxutils)
* [MAIMU](https://bitbucket.org/madmiraal/maimu)
* [MADSP](https://bitbucket.org/madmiraal/madsp)
